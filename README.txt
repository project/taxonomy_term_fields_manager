INTRODUCTION
------------

This Module is useful for listing the custom term fields on taxonomy term page.
The Module provides an interface to select the fields which you want to display on term listing page.
You can select number of fields (Maximum 3).

REQUIREMENTS
------------

This module requires the following modules:

* Taxonomy (https://www.drupal.org/project/taxonomy)

INSTALLING
----------

* Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


CONFIGURATION
-------------

1. Visit the taxonomy administration page on your site ( admin/structure/taxonomy ).
2. Click "list terms" under the operations column for the vocabulary you would like to alter the display output of.
3. Click the "Select fields" link, it will redirect to custom fields display page.
4. Under custom display settings check the fields which you want to display in Taxonomy term page and click save.
5. This functionality works only for custom fields if available.
